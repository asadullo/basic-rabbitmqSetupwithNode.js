import * as AmqpLib from 'amqplib';

class AmqpRequest {
  method: string;
  params: any;
}

export class AmqpModule {
  private static instance: AmqpModule 
  private static connection: AmqpLib.Connection;
  private static channel: AmqpLib.Channel
  private static topics = {
    external: {
      send:'mvc_core_to_external',
      listen: 'external_to_mvc_core'
    },
    other : {
      send: 'mvc_core_to_other',
      listen: 'other_to_mvc_core'
    }
  }
  private static listenOptions = {
    noAck: true
  }
  private constructor() {

  }

  public static getInstance() {
    if(!AmqpModule.instance) {
      AmqpModule.instance = new AmqpModule();
    }

    return AmqpModule.instance;
  }
  
  public async connect() {
    try{
      AmqpModule.connection = await AmqpLib.connect(`amqp://${process.env.AMQP_HOST}`);
      AmqpModule.channel = await AmqpModule.connection.createChannel();
      await this.assertQueues();
      await this.listenAll();
      return AmqpModule.connection
    }
    catch(err) {
      console.log(`Amqp Module Error : ${err}`);
      throw err;
    }
  }

  public async assertQueues() {
    AmqpModule.channel.assertQueue(AmqpModule.topics.external.send);
    AmqpModule.channel.assertQueue(AmqpModule.topics.other.send);
    AmqpModule.channel.assertQueue(AmqpModule.topics.external.listen);
    AmqpModule.channel.assertQueue(AmqpModule.topics.other.listen);
  }

  public async listenAll() {
    AmqpModule.channel.consume(AmqpModule.topics.external.listen, this.listenExternalApi, AmqpModule.listenOptions);
  }

  public async sendExternalApi(params: any, method: string) {
    try{
      let request = {
        method: method,
        params: params
      }
      console.log(`Send Amqp to Externa: ${JSON.stringify(request)}`);
      return AmqpModule.channel.sendToQueue(AmqpModule.topics.external.send, Buffer.from(JSON.stringify(request)));
    }
    catch(err) {
      console.log(`== sendExternalApi ERROR ==\n`, {message: err.message , stack: err.stack});
      return;
    }
 }

 public async listenExternalApi(msg: AmqpLib.ConsumeMessage) {
   try{
    let req: AmqpRequest = JSON.parse(msg.content.toString());
    console.log(`External Api Msg: `, req);
   }
   catch(err) {
     console.log(`== listenExternalApi ERROR ==\n`, { message: err.message, stack: err.stack });
     return;
   }
 }
}